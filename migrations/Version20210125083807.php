<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210125083807 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE guy_demarle (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, description LONGTEXT NOT NULL, created_at DATETIME NOT NULL, file_name VARCHAR(255) DEFAULT NULL, updated_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE guy_demarle_hashtag (guy_demarle_id INT NOT NULL, hashtag_id INT NOT NULL, INDEX IDX_9F334C96EAD2BE71 (guy_demarle_id), INDEX IDX_9F334C96FB34EF56 (hashtag_id), PRIMARY KEY(guy_demarle_id, hashtag_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE guy_demarle_hashtag ADD CONSTRAINT FK_9F334C96EAD2BE71 FOREIGN KEY (guy_demarle_id) REFERENCES guy_demarle (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE guy_demarle_hashtag ADD CONSTRAINT FK_9F334C96FB34EF56 FOREIGN KEY (hashtag_id) REFERENCES hashtag (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE guy_demarle_hashtag DROP FOREIGN KEY FK_9F334C96EAD2BE71');
        $this->addSql('DROP TABLE guy_demarle');
        $this->addSql('DROP TABLE guy_demarle_hashtag');
    }
}
