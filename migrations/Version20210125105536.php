<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210125105536 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE guy_demarle_hashtag');
        $this->addSql('ALTER TABLE guy_demarle ADD hashtags_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE guy_demarle ADD CONSTRAINT FK_B41330A865827D0B FOREIGN KEY (hashtags_id) REFERENCES hashtag (id)');
        $this->addSql('CREATE INDEX IDX_B41330A865827D0B ON guy_demarle (hashtags_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE guy_demarle_hashtag (guy_demarle_id INT NOT NULL, hashtag_id INT NOT NULL, INDEX IDX_9F334C96EAD2BE71 (guy_demarle_id), INDEX IDX_9F334C96FB34EF56 (hashtag_id), PRIMARY KEY(guy_demarle_id, hashtag_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE guy_demarle_hashtag ADD CONSTRAINT FK_9F334C96EAD2BE71 FOREIGN KEY (guy_demarle_id) REFERENCES guy_demarle (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE guy_demarle_hashtag ADD CONSTRAINT FK_9F334C96FB34EF56 FOREIGN KEY (hashtag_id) REFERENCES hashtag (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE guy_demarle DROP FOREIGN KEY FK_B41330A865827D0B');
        $this->addSql('DROP INDEX IDX_B41330A865827D0B ON guy_demarle');
        $this->addSql('ALTER TABLE guy_demarle DROP hashtags_id');
    }
}
