<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210125110048 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE guy_demarle DROP FOREIGN KEY FK_B41330A865827D0B');
        $this->addSql('DROP INDEX IDX_B41330A865827D0B ON guy_demarle');
        $this->addSql('ALTER TABLE guy_demarle CHANGE hashtags_id hashtag_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE guy_demarle ADD CONSTRAINT FK_B41330A8FB34EF56 FOREIGN KEY (hashtag_id) REFERENCES hashtag (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_B41330A8FB34EF56 ON guy_demarle (hashtag_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE guy_demarle DROP FOREIGN KEY FK_B41330A8FB34EF56');
        $this->addSql('DROP INDEX UNIQ_B41330A8FB34EF56 ON guy_demarle');
        $this->addSql('ALTER TABLE guy_demarle CHANGE hashtag_id hashtags_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE guy_demarle ADD CONSTRAINT FK_B41330A865827D0B FOREIGN KEY (hashtags_id) REFERENCES hashtag (id)');
        $this->addSql('CREATE INDEX IDX_B41330A865827D0B ON guy_demarle (hashtags_id)');
    }
}
