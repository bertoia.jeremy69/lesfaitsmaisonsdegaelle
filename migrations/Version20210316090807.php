<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210316090807 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE preference DROP INDEX UNIQ_5D69B05389312FE9, ADD INDEX IDX_5D69B05389312FE9 (recette_id)');
        $this->addSql('ALTER TABLE preference DROP FOREIGN KEY FK_5D69B053EAD2BE71');
        $this->addSql('DROP INDEX UNIQ_5D69B053EAD2BE71 ON preference');
        $this->addSql('ALTER TABLE preference DROP guy_demarle_id');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE preference DROP INDEX IDX_5D69B05389312FE9, ADD UNIQUE INDEX UNIQ_5D69B05389312FE9 (recette_id)');
        $this->addSql('ALTER TABLE preference ADD guy_demarle_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE preference ADD CONSTRAINT FK_5D69B053EAD2BE71 FOREIGN KEY (guy_demarle_id) REFERENCES guy_demarle (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_5D69B053EAD2BE71 ON preference (guy_demarle_id)');
    }
}
