<?php
namespace App\Command;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UserPromoteCommand extends Command
{
    protected static $defaultName = 'app:user:promote';

    private $om;

    public function __construct(EntityManagerInterface $om)
    {
        $this->om = $om;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Promouvoir un utilisateur en lui ajoutant un nouveau rôle.')
            ->addArgument('username', InputArgument::REQUIRED, 'Le pseudo de l\'utilisateur que vous voulez promouvoir.')
            ->addArgument('roles', InputArgument::REQUIRED, 'Le rôle que vous souhaitez donner à l\'utilisateur')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $username = $input->getArgument('username');
        $roles = $input->getArgument('roles');

        $userRepository = $this->om->getRepository(User::class);
        $user = $userRepository->findOneByUsername($username);

        if ($user) {
            $user->addRoles($roles);
            $this->om->flush();

            $io->success('Le rôle de l\'utilisateur a été ajouté avec succès .');
        } else {
            $io->error('Aucun utilisateur ne correspond au pseudo fourni.');
        }

        return 0;
    }
}