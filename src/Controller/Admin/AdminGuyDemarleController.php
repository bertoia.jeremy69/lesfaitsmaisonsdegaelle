<?php

namespace App\Controller\Admin;

use App\Entity\GuyDemarle;
use App\Form\GuyDemarleType;
use App\Repository\GuyDemarleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/guydemarle")
 */
class AdminGuyDemarleController extends AbstractController
{
    /**
     * @Route("/", name="admin_guydemarle_index", methods={"GET"})
     */
    public function index(GuyDemarleRepository $GuyDemarleRepository): Response
    {
        return $this->render('admin/guydemarle/index.html.twig', [
            'current_menu' => "admin_guydemarle",
            'guydemarles'     => $GuyDemarleRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="admin_guydemarle_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $guydemarle = new GuyDemarle();
        $form = $this->createForm(GuyDemarleType::class, $guydemarle);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($guydemarle);
            $entityManager->flush();

            return $this->redirectToRoute('admin_guydemarle_index');
        }

        return $this->render('admin/guydemarle/new.html.twig', [
            'current_menu' => "admin_guydemarle",
            'guydemarle'      => $guydemarle,
            'form'         => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="admin_guydemarle_show", methods={"GET"})
     */
    public function show(GuyDemarle $guydemarle): Response
    {
        return $this->render('admin/guydemarle/show.html.twig', [
            'current_menu' => "admin_guydemarle",
            'guydemarle'      => $guydemarle,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="admin_guydemarle_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, GuyDemarle $guydemarle): Response
    {
        $form = $this->createForm(GuyDemarleType::class, $guydemarle);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_guydemarle_index');
        }

        return $this->render('admin/guydemarle/edit.html.twig', [
            'current_menu' => "admin_guydemarle",
            'guydemarle'      => $guydemarle,
            'form'         => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="admin_guydemarle_delete", methods={"DELETE"})
     */
    public function delete(Request $request, GuyDemarle $guydemarle): Response
    {
        if ($this->isCsrfTokenValid('delete'.$guydemarle->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($guydemarle);
            $entityManager->flush();
        }

        return $this->redirectToRoute('admin_guydemarle_index');
    }
}
