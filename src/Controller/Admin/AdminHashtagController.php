<?php

namespace App\Controller\Admin;

use App\Entity\Hashtag;
use App\Form\HashtagType;
use App\Repository\HashtagRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/hashtag")
 */
class AdminHashtagController extends AbstractController
{
    /**
     * @Route("/", name="admin_hashtag_index", methods={"GET"})
     */
    public function index(HashtagRepository $hashtagRepository): Response
    {
        return $this->render('admin/hashtag/index.html.twig', [
            'current_menu' => "admin_hashtags",
            'hashtags'     => $hashtagRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="admin_hashtag_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $hashtag = new Hashtag();
        $form = $this->createForm(HashtagType::class, $hashtag);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($hashtag);
            $entityManager->flush();

            return $this->redirectToRoute('admin_hashtag_index');
        }

        return $this->render('admin/hashtag/new.html.twig', [
            'current_menu' => "admin_hashtags",
            'hashtag'      => $hashtag,
            'form'         => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="admin_hashtag_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Hashtag $hashtag): Response
    {
        $form = $this->createForm(HashtagType::class, $hashtag);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_hashtag_index');
        }

        return $this->render('admin/hashtag/edit.html.twig', [
            'current_menu' => "admin_hashtags",
            'hashtag'      => $hashtag,
            'form'         => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="admin_hashtag_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Hashtag $hashtag): Response
    {
        if ($this->isCsrfTokenValid('delete'.$hashtag->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($hashtag);
            $entityManager->flush();
        }

        return $this->redirectToRoute('admin_hashtag_index');
    }
}
