<?php

namespace App\Controller\Admin;

use App\Entity\Recette;
use App\Form\RecetteType;
use App\Repository\RecetteRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/recette")
 */
class AdminRecetteController extends AbstractController
{
    /**
     * @Route("/", name="admin_recette_index", methods={"GET"})
     */
    public function index(RecetteRepository $recetteRepository): Response
    {
        return $this->render('admin/recette/index.html.twig', [
            'current_menu' => "admin_recettes",
            'recettes'     => $recetteRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="admin_recette_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $recette = new Recette();
        $form = $this->createForm(RecetteType::class, $recette);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($recette);
            $entityManager->flush();

            return $this->redirectToRoute('admin_recette_index');
        }

        return $this->render('admin/recette/new.html.twig', [
            'current_menu' => "admin_recettes",
            'recette'      => $recette,
            'form'         => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="admin_recette_show", methods={"GET"})
     */
    public function show(Recette $recette): Response
    {
        return $this->render('admin/recette/show.html.twig', [
            'current_menu' => "admin_recettes",
            'recette'      => $recette,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="admin_recette_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Recette $recette): Response
    {
        $form = $this->createForm(RecetteType::class, $recette);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_recette_index');
        }

        return $this->render('admin/recette/edit.html.twig', [
            'current_menu' => "admin_recettes",
            'recette'      => $recette,
            'form'         => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="admin_recette_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Recette $recette): Response
    {
        if ($this->isCsrfTokenValid('delete'.$recette->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($recette);
            $entityManager->flush();
        }

        return $this->redirectToRoute('admin_recette_index');
    }
}
