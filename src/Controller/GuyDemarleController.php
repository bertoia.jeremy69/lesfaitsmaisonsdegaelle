<?php

namespace App\Controller;

use App\Entity\GuyDemarle;
use App\Form\GuyDemarleType;
use App\Repository\GuyDemarleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/guy/demarle")
 */
class GuyDemarleController extends AbstractController
{
    /**
     * @Route("/", name="guydemarle_index", methods={"GET"})
     */
    public function index(GuyDemarleRepository $guyDemarleRepository): Response
    {
        return $this->render('guy_demarle/index.html.twig', [
            'current_menu' => 'guydemarle',
            'guydemarles' => $guyDemarleRepository->findAll(),
        ]);
    }

    /**
     * @Route("/{id}", name="guydemarle_show", methods={"GET"})
     */
    public function show(GuyDemarle $guyDemarle): Response
    {
        return $this->render('guy_demarle/show.html.twig', [
            'current_menu' => 'guydemarle',
            'guydemarle' => $guyDemarle,
        ]);
    }

}
