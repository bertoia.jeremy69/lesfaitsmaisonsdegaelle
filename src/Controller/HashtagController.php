<?php

namespace App\Controller;

use App\Entity\Hashtag;
use App\Entity\Recette;
use App\Form\HashtagType;
use App\Entity\GuyDemarle;
use App\Repository\HashtagRepository;
use App\Repository\RecetteRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


/**
 * @Route("/hashtag")
 */
class HashtagController extends AbstractController
{
    private $em;
    private $repository;

    public function __construct(HashtagRepository $repository, EntityManagerInterface $em){
        $this->repository = $repository;
        $this->em = $em;
    }

    /**
     * @Route("/", name="hashtag_index", methods={"GET"})
     */
    public function index(HashtagRepository $repository): Response
    {
        return $this->render('hashtag/index.html.twig', [
            'hashtags' => $repository->findAll(),
        ]);
    }


    /**
     * @Route("/{id}", name="hashtag_show", methods={"GET"})
     */
    public function show(Hashtag $hashtag): Response
    {
        $repository = $this->em->getRepository(Recette::class);
        $recettes = $repository->findRecettesByHashtag($hashtag);

        $repository = $this->em->getRepository(GuyDemarle::class);
        $guydemarle = $repository->findGuyDemarleByHashtag($hashtag);

        return $this->render('hashtag/show.html.twig', [
            'hashtag'    => $hashtag,
            'recettes'   => $recettes,
            'guydemarle' => $guydemarle,
        ]);
    }

}
