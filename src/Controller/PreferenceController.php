<?php

namespace App\Controller;

use App\Entity\Recette;
use App\Entity\Preference;
use App\Form\PreferenceType;
use App\Repository\PreferenceRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/preference")
 */
class PreferenceController extends AbstractController
{
    /**
     * @Route("/", name="preference_index", methods={"GET"})
     */
    public function index(PreferenceRepository $preferenceRepository, Request $request): Response
    {
        return $this->render('preference/index.html.twig', [
            'preferences' => $preferenceRepository->findAll(),
        ]);
    }

    /**
     * @Route("/add/{id}", name="preference_add", methods={"GET","POST"})
     */
    public function new(Request $request, Recette $recette): Response
    {
        $preference = new Preference();
        $user       = $this->getUser();
        $form       = $this->createForm(PreferenceType::class, $preference);

        $form->handleRequest($request);

        $preference->setUser($user)
                    ->setRecette($recette);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($preference);
        $entityManager->flush();
        
        $referer = htmlspecialchars($request->headers->get('referer'));
        return new RedirectResponse($referer);
    }
    
    /**
     * @Route("/show/{id}", name="preference_show", methods={"GET"})
     */
    public function show(Preference $preference): Response
    {
        return $this->render('preference/show.html.twig', [
            'preference' => $preference,
        ]);
    }

    /**
     * @Route("/{id}", name="preference_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Preference $preference): Response
    {
        if ($this->isCsrfTokenValid('delete', $request->request->get('_token'))) {
            dump($preference);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($preference);
            $entityManager->flush();
        }
        
        $referer = htmlspecialchars($request->headers->get('referer'));
        return new RedirectResponse($referer);
    }
    
    
}
