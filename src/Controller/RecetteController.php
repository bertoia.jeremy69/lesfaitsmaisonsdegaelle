<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Entity\Recette;
use App\Form\CommentType;
use App\Form\RecetteType;
use App\Repository\CommentRepository;
use App\Repository\RecetteRepository;
use App\Repository\PreferenceRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/recette")
 */
class RecetteController extends AbstractController
{
    /**
     * @Route("/", name="recette_index", methods={"GET"})
     */
    public function index(RecetteRepository $recetteRepository, PreferenceRepository $preferenceRepository): Response
    {
        $user = $this->getUser();
                /* if(full_authentifié){
                    
                } */
        $preferences = $preferenceRepository->findAllByToUser($user);
        $arrayRecetteAndPreferenceID = [];
        
        
        foreach($preferences as $preference) {
            $arrayRecetteAndPreferenceID[$preference->getRecette()->getId()] = $preference->getId();
        }

        return $this->render('recette/index.html.twig', [
            'current_menu' => 'recettes',
            'recettes'     => $recetteRepository->findAllOnline(),
            'user'         => $user,
            'arrayRecetteAndPreferenceID'  => $arrayRecetteAndPreferenceID,
        ]);
    }

    /**
     * @Route("/{id}", name="recette_show", methods={"GET","POST"})
     */
    public function show(Recette $recette, CommentRepository $commentRepository, Request $request): Response
    {
        $comments = $commentRepository->findByRecette($recette);
        
        $comment = new Comment();
        $user = $this->getUser();
        $form = $this->createForm(CommentType::class, $comment);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $comment->setUser($user)
                    ->setRecette($recette);
            $entityManager->persist($comment);
            $entityManager->flush();

            return $this->redirectToRoute('recette_show', ['id' => $recette->getId()]);
        }
        return $this->render('recette/show.html.twig', [
            'current_menu' => 'recettes',
            'recette'      => $recette,
            'comments'     => $comments,
            'form'         => $form->createView(),
            'user'         => $user
        ]);
    }

    /**
     * @Route("/comment/{id}", name="recette_comment_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Comment $comment): Response
    {
        if ($this->isCsrfTokenValid('delete'.$comment->getId(), $request->request->get('_token'))) {
            $recette = $comment->getRecette();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($comment);
            $entityManager->flush();
        }

        return $this->redirectToRoute('recette_show', ['id' => $recette->getId()]);
    }

}
