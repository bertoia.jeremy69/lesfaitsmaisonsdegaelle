<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use App\Repository\UserRepository;
use App\Repository\CommentRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/user")
 */
class UserController extends AbstractController
{
    /**
     * @Route("/", name="user_dashboard", methods={"GET"})
     */
    public function index(CommentRepository $commentRepository): Response
    {
        $user = $this->getUser();
        $numberComment = $commentRepository->findNumberOfCommentByUser($user);
        
        return $this->render('user/dashboard.html.twig', [
            'user'             => $user,
            'current_menu'     => 'user',
            'current_sub_menu' => 'dashboard',
            'numberComment'    => $numberComment
        ]);
    }

    /**
     * @Route("/compte", name="user_compte", methods={"GET"})
     */
    public function compte(): Response
    {
        $user = $this->getUser();
        return $this->render('user/compte.html.twig', [
            'user' => $user,
            'current_menu' => 'user',
            'current_sub_menu' => 'compte',
        ]);
    }

    /**
     * @Route("/preference", name="user_preference", methods={"GET"})
     */
    public function preference(): Response
    {
        $user = $this->getUser();
        return $this->render('user/preference.html.twig', [
            'user' => $user,
            'current_menu' => 'user',
            'current_sub_menu' => 'preference',
        ]);
    }

    /**
     * @Route("/securite", name="user_securite", methods={"GET"})
     */
    public function securite(): Response
    {
        $user = $this->getUser();
        return $this->render('user/securite.html.twig', [
            'user' => $user,
            'current_menu' => 'user',
            'current_sub_menu' => 'securite',
        ]);
    }

    /**
     * @Route("/{id}/edit", name="user_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, User $user): Response
    {
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('user_index');
        }

        return $this->render('user/edit.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="user_delete", methods={"DELETE"})
     */
    public function delete(Request $request, User $user): Response
    {
        if ($this->isCsrfTokenValid('delete'.$user->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($user);
            $entityManager->flush();
        }

        return $this->redirectToRoute('user_index');
    }
}
