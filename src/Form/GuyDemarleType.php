<?php

namespace App\Form;

use App\Entity\Hashtag;
use App\Entity\GuyDemarle;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class GuyDemarleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('description')
            ->add('imageFile', FileType::class, [
                'required' => false
            ])
            ->add('link')
            ->add('hashtag', EntityType::class, [
                'class' => Hashtag::class, 
                'choice_label' => 'title'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => GuyDemarle::class,
            'translation_domain' => 'forms'
        ]);
    }
}
