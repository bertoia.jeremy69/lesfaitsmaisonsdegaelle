<?php

namespace App\Form;

use App\Entity\Hashtag;
use App\Entity\Recette;
use App\Form\EtapeType;
use App\Form\IngredientType;
use App\Repository\HashtagRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class RecetteType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('description')
            ->add('grade')
            ->add('person')
            ->add('imageFile', FileType::class, [
                'required' => false
            ])
            ->add('hashtags', EntityType::class, [
                'class' => Hashtag::class, 
                'choice_label' => 'title',
                'query_builder' => function (HashtagRepository $e) {
                    return $e->createQueryBuilder('h')
                        ->orderBy('h.title', 'ASC');
                },
                'multiple'=> true
            ])
            ->add('ingredients', CollectionType::class, [
				'entry_type' => IngredientType::class,
				'entry_options' => ['label' => false],
				'allow_add' => true,
				'allow_delete' => true,
				'by_reference' => false
			])
            ->add('etapes', CollectionType::class, [
				'entry_type' => EtapeType::class,
				'entry_options' => ['label' => false],
				'allow_add' => true,
				'allow_delete' => true,
				'by_reference' => false
			])
            ->add('advice', TextareaType::class, [
                'label'    => ' Avis de Gaëlle ',
                'required' => false,
            ])
            ->add('online', CheckboxType::class, [
                'label'    => ' Mettre en ligne ',
                'required' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Recette::class,
            'translation_domain' => 'forms',
        ]);
    }
}
