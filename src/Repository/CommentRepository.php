<?php

namespace App\Repository;

use App\Entity\User;
use App\Entity\Comment;
use App\Entity\Recette;
use App\Entity\GuyDemarle;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method Comment|null find($id, $lockMode = null, $lockVersion = null)
 * @method Comment|null findOneBy(array $criteria, array $orderBy = null)
 * @method Comment[]    findAll()
 * @method Comment[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CommentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Comment::class);
    }

    public function optionsResearch($research){
        return  $research->orderBy('c.created_at', 'ASC')
                        ->setMaxResults(10)
                        ->getQuery()
                        ->getResult();
    }

    /**
     * @return Comment[] Returns an array of Comment objects
    */
    public function findByRecette(Recette $recette)
    {
        $recetteComments = $this->createQueryBuilder('c')
                                ->andWhere('c.recette = :val')
                                    ->setParameter('val', $recette);
        return self::optionsResearch($recetteComments);
    }

    /**
     * @return Comment[] Returns an array of Comment objects
    */
    public function findByGuyDemarle(GuyDemarle $guyDemarle)
    {
        $guyDemarleComments = $this->createQueryBuilder('c')
                                    ->andWhere('c.guyDemarle = :val')
                                        ->setParameter('val', $guyDemarle);
        return self::optionsResearch($guyDemarleComments);
    }

    /**
     * @return integer Returns an integer of number Comment 
    */
    public function findNumberOfCommentByUser(User $user)
    {
        $UserComments = $this->createQueryBuilder('c')
                             ->andWhere('c.user = :val')
                                ->setParameter('val', $user)
                             ->getQuery()
                             ->getResult();;
        $numberComments = count($UserComments);
        
        return $numberComments;
    }

    // /**
    //  * @return Comment[] Returns an array of Comment objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Comment
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
