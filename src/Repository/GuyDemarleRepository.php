<?php

namespace App\Repository;

use App\Entity\Hashtag;
use App\Entity\GuyDemarle;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method GuyDemarle|null find($id, $lockMode = null, $lockVersion = null)
 * @method GuyDemarle|null findOneBy(array $criteria, array $orderBy = null)
 * @method GuyDemarle[]    findAll()
 * @method GuyDemarle[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GuyDemarleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, GuyDemarle::class);
    }

    /**
     * @return GuyDemarle[] Returns an array of Recette objects
     */
    public function findGuyDemarleByHashtag(Hashtag $hashtag)
    {
        $entityManager = $this->getEntityManager();

        $query = $entityManager->createQuery(' SELECT g, h
                                    FROM App\Entity\GuyDemarle g
                                    INNER JOIN g.hashtag h
                                    WHERE h.id = :id'
                                )->setParameter('id', $hashtag->getId());

        return $query->getResult();
    }

    // /**
    //  * @return GuyDemarle[] Returns an array of GuyDemarle objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('g.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?GuyDemarle
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
