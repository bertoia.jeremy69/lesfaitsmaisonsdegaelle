<?php

namespace App\Repository;

use App\Entity\Hashtag;
use App\Entity\Recette;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method Recette|null find($id, $lockMode = null, $lockVersion = null)
 * @method Recette|null findOneBy(array $criteria, array $orderBy = null)
 * @method Recette[]    findAll()
 * @method Recette[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RecetteRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Recette::class);
    }

    /**
     * @return Recette[] Returns an array of Recette objects
     */
    public function findAllOnline()
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.online = true')
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return Recette[] Returns an array of Recette objects
     */
    public function findRecettesByHashtag(Hashtag $hashtag)
    {
        $entityManager = $this->getEntityManager();

        $query = $entityManager->createQuery(' SELECT r, h
                                    FROM App\Entity\Recette r
                                    INNER JOIN r.hashtags h
                                    WHERE h.id = :id'
                                )->setParameter('id', $hashtag->getId());

        return $query->getResult();
    }

    
    // /**
    //  * @return Recette[] Returns an array of Recette objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Recette
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
